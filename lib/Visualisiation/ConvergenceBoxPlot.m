function ConvergenceBoxPlot(x,y,z,type,varargin)

switch type
    case 'normal'
        % Set limits in x and y
        x = x(:);
        y = y(:);
        X = zeros(length(x)+1,1);
        Y = zeros(length(y)+1,1);
        
        X(2:end-1,1) = (x(2:end) - x(1:end-1))/2;
        Y(2:end-1,1) = (y(2:end) - y(1:end-1))/2;
        
        X(1) = x(1)-X(2);
        Y(1) = y(1)-Y(2);
        X(end) = X(end-1);
        Y(end) = Y(end-1);
        
        X(2:end,1) = X(2:end,1) + x(:);
        Y(2:end,1) = Y(2:end,1) + y(:);
        
    case 'fixedWidth'
        
        x = x(:);
        y = y(:);
        
        X = 0 : (length(x)); X = X(:)/2;
        Y = 0 : (length(y)); Y = Y(:)/2;
        
    otherwise
        error('Plot type not known')
end


%% Specify colours
if isempty(varargin)
    cMin = min(z(:));
    cMax = max(z(:));
else
    cMin = varargin{1}(1);
    cMax = varargin{1}(2);
end

if isempty(varargin) || length(varargin) < 2
    colours = parula(255);
else
    colours = varargin{2};
%     % From [0 0 1] to [1 1 1] to [1 0 0];
%     m1 = floor(255*0.5);
%     r = (0:m1-1)'/max(m1,1);
%     g = r;
%     r = [r; ones(m1+1,1)];
%     g = [g; 1; flipud(g)];
%     b = flipud(r);
%     
%     colours = [r g b];
end

if cMin == cMax
    cR = spline([cMin, cMin + 1], [0, 0]);
    cB = spline([cMin, cMin + 1], [0, 0]);
    cG = spline([cMin, cMin + 1], [0.5, 0.5]);
else    
    c = linspace(cMin,cMax,255);
    cR = spline(c,colours(:,1));
    cG = spline(c,colours(:,2));
    cB = spline(c,colours(:,3));
end

%% Plot

XTP = 0 * x;
YTP = 0 * y;
for ii = 1 : length(x)
    for jj = 1 : length(y)
        
        LX = X(ii+1)-X(ii);
        LY = Y(jj+1)-Y(jj);
        
        X0 = X(ii);
        Y0 = Y(jj);
        
        XTP(ii) = X0 + LX/2;
        YTP(jj) = Y0 + LY/2;
        
        Z = z(jj,ii);
        
        if ~isnan(Z)
            if Z < cMin
                Z = cMin;
            elseif Z > cMax
                Z = cMax;
            end
            
            hold on
            
            clr = [fnval(cR,Z),fnval(cG,Z),fnval(cB,Z)];
            clr(clr < 0) = 0;
            clr(clr > 1) = 1;
            
            rectangle('Position',[X0,Y0,LX,LY], ...
                'FaceColor', clr, ...
                ...%'EdgeColor',[fnval(cR,Z),fnval(cG,Z),fnval(cB,Z)])
                'EdgeColor','k')
        end
    end
end

xticks(XTP)
yticks(YTP)
xticklabels(num2str(x))
yticklabels(num2str(y))

axis tight
box on

if cMin ~= cMax
    colorbar
    caxis([cMin,cMax])
end

end
