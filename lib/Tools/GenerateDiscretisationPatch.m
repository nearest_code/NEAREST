function x = GenerateDiscretisationPatch(l,h,x0,zFunc)

% Discretise as circle in (x,y) plane
r = 0:h:l; r = r(2:end);

% Superfine grid
ph = linspace(0,2*pi,round(10/h))';

x1 = [];
x2 = [];

for ii = 1 : length(r)
    
    candidatePoints = [r(ii) * cos(ph) , r(ii) * sin(ph)];
    
    for jj = 1 : length(ph)
        dist = (candidatePoints(jj,1) - ...
            candidatePoints(jj+1:end,1)).^2 + ...
            (candidatePoints(jj,2) - ...
            candidatePoints(jj+1:end,2)).^2;
        
        ind = jj+1 : jj+1 + length(dist) - 1;
        
        % Remove points closer than h away
        ind(dist < h) = [];
       
        if isempty(ind)
            candidatePoints(jj+1 : end, :) = [];
            break
        end
        
        ind = ind(1);
        
        candidatePoints(jj + 1 : ind-1,:) = [];
        
    end
    
    % Make sure last point not too close
    if (candidatePoints(1,1)-candidatePoints(end,1))^2 ...
            + (candidatePoints(1,2)-candidatePoints(end,2))^2 ...
            < h^2/2
        candidatePoints(end,:) = [];
    end
        
    x1 = [x1 ; candidatePoints(:,1)]; %#ok<AGROW>
    x2 = [x2 ; candidatePoints(:,2)]; %#ok<AGROW>    
    
end

% Translate
x1 = x1 + x0(1);
x2 = x2 + x0(2);

% Calculate z-positions
x3 = zFunc(x1,x2);

% Remove points which lie outside boundaries
x = [x1(:),x2(:),x3(:)];
x(imag(x3(:)) ~= 0,:) = [];

end
