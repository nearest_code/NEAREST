function fourierCoefs = CalculateFourierCoefficients(f,nCoefs)%,t,fTest)

% Fourier transform
[nT,nF] = size(f);
FC = fft(f); FC = FC(1:floor(nT/2),:);

% Keep largest NCOEFS modes
[~,jj] = sort(abs(FC(2:end,:)),1,'descend');
kk = repmat(1:nF,nCoefs,1);
ind = sub2ind(size(FC),1+jj(1:nCoefs,:),kk);

% Fourier coefficients
aN = reshape(FC(ind(:)),[nCoefs,nF]);

% Add zero mode and scale with nT
fourierCoefs.aN = [FC(1,:)/nT ; aN / nT * 2];

% Requencies
fourierCoefs.fN = [zeros(1,nF) ; jj(1:nCoefs,:)];

% % Recreate signal (testing)
% ft = 0*fTest;
% for ii = 1:nCoefs+1
%     ft = ft + fourierCoefs.aN(ii,:) .* exp(1i*2*pi*t'*fourierCoefs.fN(ii,:));
% end
% ft = real(ft);
% max(max(abs(ft-fTest)))
% pcolor(abs(ft-fTest))
% shading interp
% colorbar

end
