%REMOVECLOSEPOINTS Removes points in X closer than MINDIST from X0
function x = RemoveClosePoints(x,x0,minDist)

[x1,x2,x3] = ExtractComponents(x);

dist = (x1-x0(1)).^2 + (x2-x0(2)).^2 + (x3-x0(3)).^2;

x1( dist <= minDist^2 ) = [];
x2( dist <= minDist^2 ) = [];
x3( dist <= minDist^2 ) = [];

x = [x1(:) ; x2(:) ; x3(:)];

end
