function NNMatrices = InitialiseNNMatrices(problemFlag,swimmer, ...
    boundary,blockSize,varargin)

if isempty(varargin)
    t = 0;
else
    t = varargin{1};
end


switch problemFlag
    case 'swimming'
        % Construct swimmer NN matrices
        nSw = length(swimmer);
        NNSwi = cell(nSw,1);
        for iSw = 1 : nSw
            
            % Get swimmer points
            [xForce,~,xQuad] = swimmer{iSw}.fn(t,swimmer{iSw}.model);
            
            % Get body frame
            b30 = cross(swimmer{iSw}.b10,swimmer{iSw}.b20);
            B = [swimmer{iSw}.b10(:), swimmer{iSw}.b20(:), b30(:)];
            
            % Rotate points
            xForce = ApplyRotationMatrix(B,xForce);
            xQuad = ApplyRotationMatrix(B,xQuad);
            
            % Translate points
            xForce = TranslatePoints(xForce,swimmer{iSw}.x0);
            xQuad = TranslatePoints(xQuad,swimmer{iSw}.x0);
            
            % Create nearestNeighbour matrix
            NNSwi{iSw} = NearestNeighbourMatrix(xQuad(:),xForce(:), ...
                blockSize);
        end
        
        % Merge Swimmer NN matrices
        NNSw = NNSwi{1};
        for iSw = 2 : nSw
            NNSw = MergeNNMatrices(NNSw, NNSwi{iSw});
        end
        
        % Create boundary NN Matrix
        NNBnd = [];
        if ~isempty(boundary)
            [xForce,xQuad]   = boundary.fn(boundary.model);
            
            NNBnd = NearestNeighbourMatrix(xQuad,xForce, ...
                blockSize);
        end
        
        % Create full NN Matrix
        NN = MergeNNMatrices(NNSw,NNBnd);
        
        % Output
        NNMatrices = {NN, NNSw};        

    otherwise
        error('Problem type not known')
end

end