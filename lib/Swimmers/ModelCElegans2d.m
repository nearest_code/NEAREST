function [x,v,X] = ModelCElegans2d(t,model)
% generates discretisation of a model C. elegans, griddedInterpolant body
%
% t - time
% model.f - number of force points along body midpoint
% model.q - number of quadrature points along body midpoint
%
% model.L0 - length of body
% model.r0 - max body radius
% model.h0 - length of head
% model.t0 - length of tail section
%
% model.F  - F{1} is x-interpolant, F{2} is y-interpolant

%% Coarse grid - position and velocity
% Get midpoints along body
s = linspace(0,1,model.fR);
dt=0.001;
tt=[t-dt/2  t  t+dt/2];
[sg,tg] = ndgrid(s,tt);

% theta = model.tangentAngleFn(sg,mod(tg,1));
[x1,x2] = CalcxyFromPlanarInterp(sg,mod(tg,1),model.F);

% Calculate velocities of kept points
v1 = (x1(:,3) - x1(:,1))/dt;
v2 = (x2(:,3) - x2(:,1))/dt;
v3 = 0*v1;

% Output
x = [x1(:,2) ; x2(:,2) ; 0*x1(:,2)];
v = [v1(:) ; v2(:) ; v3(:)];

%% Fine grid
% Get midpoints along body
S = linspace(0,1,model.qR);
[Sg,Tg] = ndgrid(S,t);

[X1,X2] = CalcxyFromPlanarInterp(Sg,mod(Tg,1),model.F);

X = [X1(:) ; X2(:) ; 0*X1(:)];

end
