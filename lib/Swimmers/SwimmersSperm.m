%SWIMMERSPERM.M SETS UP SWIMMER STRUCTS FOR MULTI-SPERM 
%
% Inputs:
%	NSPERM 		- Number of swimmers
%	SPERMMODEL 	- Function handle for generating sperm discretisaion
%	BEATMODEL 	- Function handle for sperm flagellar beat
%	SPERMSIZE 	- Size parameters for sperm head in multiples
%					of the flagellum length [a1,a2,a3]
%	SPERMDISC 	- Number of discretisation points for the sperm 
%					[nTail,nHead,qTail,qHead]
%	NBEATS 		- Number of beats to set up sperm
%   X0          - Initial location in space 
%   BODYFRAME   - Body frame = [Direction, normal, binormal];
%	ARGS 		- Struct for changing sperm beat parameters
%					ARGS{ISW}.PHASE, ARGS{ISW}.K sets the phase and wavenumber
%
function swimmer = SwimmersSperm(nSperm,spermModel,beatModel,spermSize, ...
    spermDisc,nBeats,x0,bodyFrame,args)

% Initialise swimmers
swimmer = cell(nSperm,1);

% Discretise S and T for construction of sperm-wave interpolant
s = linspace(0, 1, 30);
dt = 0.05 * 2 * pi;
t = 0 : dt : 2*pi*nBeats;
[S,T] = ndgrid(s,t);

% Set up swimmer struct
for iSw = 1 : nSperm
	swimmer{iSw} = struct( ...
		'fn', 	 	spermModel, ...
        'x0',       x0{iSw}, ...
        'b10',      bodyFrame{iSw}(:,1), ...
        'b20',      bodyFrame{iSw}(:,2), ...
		'model', 	struct( ...
			'F', 	[], ...
			'ns', 	spermDisc(1), ...
			'nh', 	spermDisc(2), ...
			'Ns',	spermDisc(3), ...
			'Nh',	spermDisc(4), ...
			'a1',	spermSize(1), ...
			'a2',	spermSize(2), ...
			'a3',	spermSize(3)));
    
    swimmer{iSw}.model.F = ...
        ConstructInterpolantFromxyForm(S,T,beatModel,args{iSw});
        
end

end
