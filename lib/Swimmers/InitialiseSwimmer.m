function swimmer = InitialiseSwimmer(swimmerType, varargin)

switch swimmerType
    case 'sperm'
        
        swimmer = SwimmersSperm(varargin{1},varargin{2},varargin{3}, ...
            varargin{4},varargin{5},varargin{6},varargin{7}, ...
            varargin{8},varargin{9});
        
    case 'chlamy'
        
        swimmer = SwimmersChlamy(varargin{1},varargin{2},varargin{3}, ...
            varargin{4},varargin{5},varargin{6},varargin{7}, ...
            varargin{8},varargin{9});  
        
    case 'celegans'
        
        swimmer = SwimmersCElegans(varargin{1},varargin{2},varargin{3}, ...
            varargin{4},varargin{5},varargin{6},varargin{7});  
        
    otherwise
        error('Swimmer type not known')
end

end
