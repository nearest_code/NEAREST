function r = GetCElegansRadius(s,model)

%% Set up radius function
%s = linspace(0,1,1e3);
s = s(:);
sH = s(s < model.H0);
sT = s(s > model.L0 - model.T0);

sB = s; sB(sB < model.H0) = []; sB(sB > model.L0 - model.T0) = [];

% rH
dumSH = linspace(0,model.H0,1e3);
dumRH = model.R0*sqrt(1 - (dumSH./model.H0).^2);
dumRH = spline(dumSH,fliplr(dumRH));

% rT
dumST = linspace(0,model.T0,1e3);
dumRT = model.R0*sqrt(1 - (dumST./model.T0).^2);
dumRT = spline(model.L0-fliplr(dumST),dumRT);

rH = fnval(dumRH,sH);
%rT = (model.L0 - sT)/model.T0 * model.R0;
rT = fnval(dumRT,sT);
rB = model.R0 + 0 * sB;

r = [rH(:); rB(:); rT(:)];

% % Arclength of radius function
% s = s';
% a = (s(2:end)-s(1:end-1)).^2 + (r(2:end)-r(1:end-1)).^2;
% a = cumsum([0;sqrt(a)]);
% 
% % Spline radius function
% R = spline(a,r);
% 
% %% Sample nS points linearly spaced along the boundary
% s = linspace(min(a),max(a),nS);
% r = fnval(R,s);
end