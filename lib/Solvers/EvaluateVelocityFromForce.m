function u=EvaluateVelocityFromForce(xf,X,x,f, ...
    epsilon,domain,blockSize,procFlag,varargin)

% input:
% xf - field points
% X  - quadrature grid
% x  - force grid
% f  - forces
% epsilon - regularization parameter
% domain - i for stokeslet, h for blakelet
% blocksize - memory allocation for building matrix
% procFlag - cpu or gpu
% varargin{1} - NN matrix
%
% output:
% u  - velocity field at xf

if ~isempty(varargin)
    % supplied nearest-neighbour matrix
    NN=varargin{1};
end

if isempty(varargin)
    [A,~]=AssembleStokesletMatrix(xf,X,x,epsilon,domain, ...
        blockSize,procFlag);
else
    [A,~]=AssembleStokesletMatrix(xf,X,x,epsilon,domain, ...
        blockSize,procFlag,NN);
end

u=A*f;

switch procFlag
    case 'gpu'
        u = gather(u);
end

end
