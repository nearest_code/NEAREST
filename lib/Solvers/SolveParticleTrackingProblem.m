function sol = SolveParticleTrackingProblem(tRange,p0,fC, ...
    stationaryBoundary,movingBoundary,epsilon,domain,blockSize, ...
    procFlag,saveDir,saveName)

switch solveFlag
    case 'ode45'
        sol = ode45(@(T,P) EvaluateVelocityFromFourier( ...
            stationaryBoundary,movingBoundary,fC,P,T,epsilon, ...
            domain,blockSize,procFlag),tRange,p0(:),odeOpts);
        
    case 'ode113'
        sol = ode113(@(T,P) EvaluateVelocityFromFourier( ...
            stationaryBoundary,movingBoundary,fC,P,T,epsilon, ...
            domain,blockSize,procFlag),tRange,p0(:),odeOpts);
        
    otherwise
        error('Solve flag not known')
end

%% Save output
t = linspace(tRange(1),tRange(2),1e2);
p = deval(sol,t);

particleTracks = cell(1,3);
particleTracks{1,1} = t;
particleTracks{1,2} = p';
particleTracks{1,3} = sol;
ParSaveTrack(saveName,saveDir,particleTracks);

end
