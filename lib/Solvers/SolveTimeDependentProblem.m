% Solves time-varying nearest-neighbour problem
%
% input:
%   x00        Initial position vector
%   b10, b20   Initial basis vectors (third basis vector is calculated via
%              cross product)
%   tRange     time range over which to calculate trajectory
%   swimmer    Structure describing how to calculate swimmer shape and
%              kinematics
%   boundary   Structure describing boundary
%   epsilon    regularisation parameter
%   domain     switch for use of image systems
%              'i' = use infinite fluid (usual)
%              'h' = use half space images (Ainley/Blakelet)
%   blockSize  memory size for stokeslet matrix block assembly in GB.
%              0.2 is a safe choice.
%   varargin   if varargin{1}='f' then includes force components
%
function varargout = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
    domain,blockSize,solveFlag,problemFlag,procFlag,varargin)


switch problemFlag
    case 'swimming'
        
        swimmer = vargsIn{1};
        boundary = vargsIn{2};
        NNMatrices = vargsIn{3};
        
        if nargout == 1
            varargout{1} = SolveSwimmingProblem(tRange, ...
                swimmer,boundary,epsilon,domain,blockSize,solveFlag, ...
                procFlag,NNMatrices,varargin);
        else
            [varargout{1},varargout{2}] = SolveSwimmingProblem(tRange, ...
                swimmer,boundary,epsilon,domain,blockSize,solveFlag, ...
                procFlag,NNMatrices,varargin);
        end
        
    case 'movingBoundaryProblem'
        
        stationaryBoundary = vargsIn{1};
        movingBoundary = vargsIn{2};
        nFCoef = vargsIn{3};
        nPp = vargsIn{4};
        
        [fourierCoefs,f] = SolveMovingBoundaryForces(t,movingBoundary, ...
            stationaryBoundary,epsilon,nFCoef,domain,nPp,blockSize,procFlag);
        
        varargout{1} = fourierCoefs;
        varargout{2} = f;
        
        
    case 'particleTrackingFourier'
        
        stationaryBoundary = vargsIn{1};
        movingBoundary = vargsIn{2};
        fC = vargsIn{3};
        p0 = vargsIn{4};
        saveDir = vargsIn{5};
        saveName = vargsIn{6};
        
        if nargout > 1
            warning(['PROBLEMFLAG = PARTICLETRACKINGFOURIER ' ...
                'only outputs solution struct'])
        end
        
        varargout{1} = SolveParticleTrackingProblem(tRange,p0,fC, ...
            stationaryBoundary,movingBoundary,epsilon,domain,blockSize, ...
            procFlag,saveDir,saveName);
        
    otherwise
        error('Problem type not known')
end

end
