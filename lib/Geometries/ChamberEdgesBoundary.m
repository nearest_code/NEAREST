function [x,X]=ChamberEdgesBoundary(model)

% creates cuboidal chamber boundary with open ends
% Discretisations are equalLy spaced with step
% number of points in each direction are
%   model.hx , model.hy , model.hz (coarse)
%   model.qx , model.qy , model.qz (coarse)
%
% dimensions model.Lx by model.Ly by model.Lz
%
% model.O - centre of chamber
%
%% Create vertical faces in (y,z) plane
% Coarse grid
vFace = CreateSquareSurface(model.hz,model.hy);
[vFace3,vFace2,vFace1] = ExtractComponents(vFace); % x -> z, z -> x

% Scale lengths
vFace2 = vFace2 * model.Ly;
vFace3 = vFace3 * model.Lz;

vFace = [vFace1(:) ; vFace2(:); vFace3(:)];


% Fine grid
VFace = CreateSquareSurface(model.qz,model.qy);
[VFace3,VFace2,VFace1] = ExtractComponents(VFace); % x -> z, z -> x

% Scale lengths
VFace2 = VFace2 * model.Ly;
VFace3 = VFace3 * model.Lz;

VFace = [VFace1(:) ; VFace2(:); VFace3(:)];

%% Translate points and combine faces
vTranslate = [model.Lx/2; 0; 0];
vFace = MergeVectorGrids(TranslatePoints(vFace,model.O + vTranslate), ...
    TranslatePoints(vFace,model.O - vTranslate));
VFace = MergeVectorGrids(TranslatePoints(VFace,model.O + vTranslate), ...
    TranslatePoints(VFace,model.O - vTranslate));

%% Merge faces without duplicating points

x = vFace(:);
X = VFace(:);

end