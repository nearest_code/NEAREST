%PIPEBOUNDARY
%
function [x,X] = PipeBoundary(model)

% model.a - pipe radius
%       l - pipe length
%       o - pipe origin
%       b - direction of pipe
%      .hf - discretisation spacing for force points
%      .hq - discretisation spacing for quadrature points
%      .nth - number of force points around pipe NOT USED
%      .nz - number of force points along pipe NOT USED
%      .Nth - number of quadrature points around pipe NOT USED
%      .Nz - number of quadrature points along pipe NOT USED
%% Force points
% Discretise a single ring
dth = model.hf / model.a;
th = 0 : dth : 2*pi;
dth1 = 2*pi - th(end);
if dth1 < (model.hf/model.a)/2 || dth1 == 0
    th = th(1:end-1);
end

% Discretise length
x1 = 0 : model.hf : model.l;

dx1 = model.l - x1(end);
if dx1 > 0    
   if  dx1 < model.hf/2
       x1(end) = model.l;
   else
       x1 = [x1(:) ; model.l];
   end    
end

% Create points
x2 = model.a * sin(th(:));
x3 = model.a * cos(th(:));

% Repeat over all points
x2 = repmat(x2,length(x1),1);
x3 = repmat(x3,length(x1),1);
x1 = kron(x1(:),ones(length(th),1));

% Get rotation matrix
a = [1;0;0];
b = model.b(:) / sqrt(sum(model.b.^2));

if isequal(a,b) || isequal(a,-b)
    R = eye(3);
else
    R = 2*(a+b)*(a+b)'/((a+b)'*(a+b)) - eye(3);       
end

% Rotate points to be along desired direction
points = R * [x1,x2,x3]';  

% Combine for extraction
x = [points(1,:),points(2,:),points(3,:)]';

%% Quadrature points
% Discretise a single ring
Dth = model.hq / model.a;
Th = 0 : Dth : 2*pi;
Dth1 = 2*pi - Th(end);
if Dth1 < (model.hq/model.a)/2 || Dth1 == 0
    Th = Th(1:end-1);
end

% Discretise length
X1 = 0 : model.hq : model.l;

Dx1 = model.l - X1(end);
if Dx1 > 0    
   if  Dx1 < model.hq/2
       X1(end) = model.l;
   else
       X1 = [X1(:) ; model.l];
   end    
end

% Create points
X2 = model.a * sin(Th(:));
X3 = model.a * cos(Th(:));

% Repeat over all points
X2 = repmat(X2,length(X1),1);
X3 = repmat(X3,length(X1),1);
X1 = kron(X1(:),ones(length(Th),1));

% Rotate points to be along desired direction
Points = R * [X1,X2,X3]'; 

% Combine for extraction
X = [Points(1,:),Points(2,:),Points(3,:)]';

end
