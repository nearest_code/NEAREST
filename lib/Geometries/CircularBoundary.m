%CIRCULARBOUNDARY creates a uniform-area disc discretisation
%
% model.r - disc radius
%       o - disc origin
%       b - direction of pipe
%      .nR - number of rings of force points around disc
%      .NR - number of rings of quadrature points around disc
%
function [x,X,a,A] = CircularBoundary(model)

%% Discretise force points
% Number of rings
dr = model.r / model.nR;
r = 0 : dr : model.r;
hdr = dr/2;

% Nuumber of cells in each ring
k = NaN*r;
k(2) = 1;
for ii = 3 : model.nR + 1
    k(ii) = (r(ii).^2 - r(ii-1).^2)./(r(ii-1).^2 - r(ii-2).^2)...
        .* k(ii-1);
end
k = round(k);

% Area of each cell
a = pi*(r(end)^2-r(end-1)^2)/k(end);

% Cell locations
nCells = sum(k(2:end));
x1 = zeros(nCells,1);
x2 = zeros(nCells,1);
x3 = zeros(nCells,1);
for ii = 3 : model.nR + 1
    th = linspace(0,2*pi,k(ii)+1);
    th = th(1:end-1);
    hdth = (th(2)-th(1))/2;
    
    x1(sum(k(2:ii-1)) + 1 : sum(k(2:ii))) = ...
        (r(ii)-hdr) * cos(th+hdth);
    x2(sum(k(2:ii-1)) + 1 : sum(k(2:ii))) = ...
        (r(ii)-hdr) * sin(th+hdth);
end

% Get rotation matrix 
b1 = [0;0;1];
b2 = model.b(:) / sqrt(sum(model.b.^2));

if isequal(b1,b2) || isequal(b1,-b2)
    RotMat = eye(3);
else
    RotMat = 2*(b1+b2)*(b1+b2)'/((b1+b2)'*(b1+b2)) - eye(3);       
end

% Rotate points to be alnog desired direction
points = RotMat * [x1,x2,x3]';

% Translate
points = points + model.o(:);

% Combine for extraction
x = [points(1,:),points(2,:),points(3,:)]';

%% Discretise quadrature points
% Number of rings
dR = model.r / model.NR;
R = 0 : dR : model.r;
hdR = dR/2;

% Nuumber of cells in each ring
K = NaN*R;
K(2) = 1;
for ii = 3 : model.NR + 1
    K(ii) = (R(ii).^2 - R(ii-1).^2)./(R(ii-1).^2 - R(ii-2).^2)...
        .* K(ii-1);
end
K = round(K);

% Area of each cell
A = pi*(R(end)^2-R(end-1)^2)/K(end);

% Cell locations
NCells = sum(K(2:end));
X1 = zeros(NCells,1);
X2 = zeros(NCells,1);
X3 = zeros(NCells,1);
for ii = 3 : model.NR + 1
    th = linspace(0,2*pi,K(ii)+1);
    th = th(1:end-1);
    hdth = (th(2)-th(1))/2;
    
    X1(sum(K(2:ii-1)) + 1 : sum(K(2:ii))) = (R(ii)-hdR) * cos(th+hdth);
    X2(sum(K(2:ii-1)) + 1 : sum(K(2:ii))) = (R(ii)-hdR) * sin(th+hdth);
end

% Rotate points to be alnog desired direction
Points = RotMat * [X1,X2,X3]';

% Translate
Points = Points + model.o(:);

% Combine for extraction
X = [Points(1,:),Points(2,:),Points(3,:)]';

end
