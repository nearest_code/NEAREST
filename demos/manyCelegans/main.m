%%
%

pLib = genpath('../../lib/');
addpath(pLib)

%% Run simulations
% Number of points per swimmer
N = 2.^(3:7);

% Run simulations on GPU
TimeSimCElegans('gpu',N);

% Run simulations on CPU
TimeSimCElegans('cpu',N);

%%
rmpath(pLib)
