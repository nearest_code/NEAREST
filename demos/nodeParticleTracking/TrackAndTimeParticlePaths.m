function simTime = TrackAndTimeParticlePaths(p0,nP,fC,movingBoundary, ...
    stationaryBoundary,nBeats,epsilon,domain,blockSize,procFlag, ...
    saveDir,saveName)

%% Set particle release height
releaseHeight = 0.2; % Height to release particles at
p0 = p0(1:nP,:);
p0(:,3) = releaseHeight + GetBoundaryPoint(p0,movingBoundary.model(1));

%% Time and track particle(s)
clear textprogressbar
odeOpts = odeset('OutputFcn',@odetpbar);
%odeOpts = [];

simTimer = tic;
sol = ode45(@(T,P) EvaluateVelocityFromFourier(stationaryBoundary, ...
    movingBoundary,fC,P,T,epsilon,domain, ...
    blockSize,procFlag),[0,nBeats],p0(:),odeOpts);
simTime = toc(simTimer);

%% Save output
t = linspace(0,nBeats,1e2);
p = deval(sol,t);

particleTracks = cell(1,3);
particleTracks{1,1} = t;
particleTracks{1,2} = p';
particleTracks{1,3} = sol;
ParSaveTrack(saveName,saveDir,particleTracks);

end
