function timeSim = TimeSimSperm(procFlag,varargin)
% Run for various choices of discretisation
if isempty(varargin)
    NH = [3,4,5,6];
else
    NH = varargin{1};
end

fprintf('Running simulation with 9 sperm between two plates on %s\n',procFlag)

timeSim = NaN(length(NH),1);

%% Setup
nSperm = 9;
nBeats = 3;

L0 = 45; %um
tRange = [0, 2*pi*nBeats];
epsilon=0.25/L0;
domain='i';

boundary = struct(          ...
    'fn',   @PlaneBoundary2,... % 2 Rectangular boundaries
    'model', struct(        ...
    'h',    0.4,        ...
    'nx',   15,         ...
    'ny',   16,         ...
    'Nx',   60,         ...
    'Ny',   64,         ...
    'Lx',   4,          ...
    'Ly',   4.5,          ...
    'O',    [-1.25;-0.75;-0.2])    ...
    );

try
    load('swimmerSperm.mat','swimmer')
catch
    swimmer = InitialiseManySperm(NH(1),L0,epsilon,nBeats);
    
    save('swimmerSperm.mat','swimmer');
end

%%
for iN = 1 : length(NH)

    fprintf('\tSimulation %i of %i: N = %i\n',iN,length(NH),NH(iN))
    
    switch procFlag
        case 'cpu'
            blockSize = 4;
        case 'gpu'
            blockSize = [4,0];
    end
    
    %% Initialise swimmer
    nT = 40;
    discrFnc = @(nH) [nT,nH,nT*4,nH*4];
    discr = discrFnc(NH(iN));
    
    for iS = 1 : nSperm
        swimmer{iS}.model.ns = discr(1);
        swimmer{iS}.model.nh = discr(2);
        swimmer{iS}.model.Ns = discr(3);
        swimmer{iS}.model.Nh = discr(4);
    end
    
    %% Start timer
    %     profile clear
    %     profile on
    odeSolTimer = tic;
    
    %% NN matrix
    NNMatrices = InitialiseNNMatrices('swimming',swimmer, ...
        boundary,blockSize);
    
    %% Solve
    vargsIn = {swimmer,boundary,NNMatrices};
    [t,z] = SolveTimeDependentProblem(vargsIn,tRange,epsilon, ...
        domain,blockSize,'ode45','swimming', ...
        procFlag); %#ok<ASGLU>
    
    %% Time
    timeSim(iN) = toc(odeSolTimer);
    fprintf('\tCompleted in %f seconds\n\n',timeSim(iN))
    
    switch procFlag
        case 'cpu'
            timeSpermCPU = timeSim;
            if length(varargin) < 2
                save('spermSimCpu_progress.mat','NH','timeSpermCPU')
            else
                save(varargin{2})
            end
            
        case 'gpu'
            timeSpermGPU = timeSim;
            
            if length(varargin) < 2
                save('spermSimGpu_progress.mat','NH','timeSpermGPU')
            else
                save(varargin{2})
            end
    end
end

switch procFlag
    case 'cpu'
        timeSpermCPU = timeSim;
        if length(varargin) < 2
            save('spermSimCpu.mat','NH','timeSpermCPU')
        else
            save(varargin{2})
        end
        
    case 'gpu'
        timeSpermGPU = timeSim;
        
        if length(varargin) < 2
            save('spermSimGpu.mat','NH','timeSpermGPU')
        else
            save(varargin{2})
        end
end

end
