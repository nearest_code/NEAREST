function swimmer = InitialiseManySperm(NH,L0,epsilon,nBeats)

try
    load('manySpermSwimmers.mat','th0','k','phi0')
    
    nSperm = numel(th0);
catch
    nSperm = 15;
    
    % Get rotation thetas
    th0 = 2*pi*rand(nSperm,1);
    
    % Get phase angles
    phi0 = 2*pi*rand(nSperm,1);
    
    % Get wave numbers
    k = pi + 2*pi*rand(nSperm,1);
    
    save('manySpermSwimmers.mat','th0','k','phi0');
end

bodySize = [2.0/L0, 1.6/L0, 1.0/L0];

nT = 40;
discrFnc = @(nH) [nT,nH,nT*4,nH*4];

discr = discrFnc(NH);

% Body frame angle
bodyFrame = cell(nSperm,1);
for ii = 1 : nSperm
    bodyFrame{ii} = RotationMatrix(th0(ii),3);
end

% x0
X0 = [-2,-1,0,1,2];
Y0 = [-2,0,2];
[X0,Y0] = meshgrid(X0,Y0);

x0 = cell(nSperm,1);
for ii = 1 : nSperm
    x0{ii} = [X0(ii) - 0.5*cos(th0(ii)+pi/6); ...
        Y0(ii) - 0.5*sin(th0(ii)+pi/6) ; 0];
end

% Phase
args = cell(nSperm,1);
for ii = 1 : nSperm
    args{ii} = struct('phase',phi0(ii),'k',k(ii));
end

%% Construct swimmers
swimmer = SwimmersSperm(nSperm,@ModelSpermGI, ...
    @WaveSpermDKAct,bodySize,discr,nBeats, ...
    x0,bodyFrame,args);

for ii = 1 : nSperm
    swimmer{ii}.model.epsilon = epsilon;
end

%% Plot
testPlots = 1;
if testPlots
    nSw = length(swimmer);
    
    figure(1)
    clf
    
    % Calculate DOF
    DOF = 0;
    for iSw = 1 : nSw
        [xForce,~,~] = swimmer{iSw}.fn(0,swimmer{iSw}.model);
        DOF = DOF + length(xForce);
    end

    z0 = zeros(9*nSw + DOF, 1);
    for iSw = 1 : nSw
        z0(iSw)         = swimmer{iSw}.x0(1);
        z0(nSw   + iSw) = swimmer{iSw}.x0(2);
        z0(2*nSw + iSw) = swimmer{iSw}.x0(3);
        z0(3*nSw + iSw) = swimmer{iSw}.b10(1);
        z0(4*nSw + iSw) = swimmer{iSw}.b10(2);
        z0(5*nSw + iSw) = swimmer{iSw}.b10(3);
        z0(6*nSw + iSw) = swimmer{iSw}.b20(1);
        z0(7*nSw + iSw) = swimmer{iSw}.b20(2);
        z0(8*nSw + iSw) = swimmer{iSw}.b20(3);
    end
    
    xForce = GetSwimmerPoints(swimmer,z0,0,'cpu');
    [x1,x2,x3]=ExtractComponents(xForce);
    plot3(x1,x2,x3,'.')
    hold on
    axis equal
    view(0,90)
    pause
end

end
