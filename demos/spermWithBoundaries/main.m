%%
%

pLib = genpath('../../lib/');
addpath(pLib)

%% Run simulations
% Number of points per swimmer head (N = 6nh^2)
NH = 3:6;

% Run simulations on GPU
TimeSimSperm('gpu',NH);

% Run simulations on CPU
TimeSimSperm('cpu',NH);

%%
rmpath(pLib)
